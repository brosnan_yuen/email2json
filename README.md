# EMAIL2JSON

**Succeeded by [EMAIL2DATABASE](https://github.com/brosnanyuen/EMAIL2DATABASE)**

Downloads emails and formats them into JSON files. [GPLv3 License](LICENSE)

Binary files found in [Releases](../../releases).
~~~
./EMAIL2JSON <config.json>
~~~

## Example of downloaded file
~~~
{
    "Date": "Mon, 7 Jan 2015 21:38:22 +0000",
    "From": "Foo Bar <foobar@mail.com>",
    "Subject": "How to program in C++",
    "To": "\"me@mail.com\" <me@mail.com>",
    "Message":["Hi me,\n do you know how to programm in C++?\nThanks\n"]
}
~~~

## Example of config file
~~~
{
	"Protocol":"IMAP4",
	"URL":"mail.com",
	"Mail_Directory":"INBOX",
	"Username":"me",
	"Password":"password",
	"Retrieve":"All",
	"Local_Directory":"/mail/",
	"Header_Filters":["Date","Subject","From","To"]
}
~~~

## Features
  * IMAP4/POP3
  * SSL
  * Parallel downloads
  * Built with Qt and libcurl
  * Filtering

## Config file specification
**Protocol**	IMAP4 or POP3

**URL**		Domain name (e.g. gmail.com, mail.yahoo.com)

**Mail_Directory**	Directory of mail (e.g. SENT,SPAM)

**Username**	Username to login

**Password**	Password for account

**Retrieve**	Number of emails to retrieve

**Local_Directory**	Directory to place json files in

**Header_Filters**	Names of header information to keep

## Building source code
Download and install [libcurl](http://curl.haxx.se/libcurl/) and [QtCreator](http://www.qt.io/)

Open up in QtCreator and build source
