#include <QCoreApplication>
#include "jsonhandle.h"
#include "errorhandle.h"
#include "emailhandle.h"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    if (argc < 2)
    {
        print_error(ca_error_type::NO_ARGUMENTS);
    }

    jsonhandle config(argv[1]);
    emailhandle mailbox(config.string_parse(QStringList() << "Protocol" << "URL" << "Mail_Directory" << "Username" << "Password" << "Retrieve" << "Local_Directory"),config.get_string_array("Header_Filters"));
    mailbox.get_all_email();
    exit(EXIT_SUCCESS);
    return a.exec();


}
