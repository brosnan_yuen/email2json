#include "emailhandle.h"
#include "errorhandle.h"
#include "jsonhandle.h"



emailhandle::emailhandle(QStringList config,QStringList filter_list)
{
    filter_keywords = filter_list;
    QStringList protocols = (QStringList() <<  "IMAP4" << "POP3");
    user = config.at(3);
    pass = config.at(4);
    local_dir = config.at(6);
    switch (protocols.indexOf(config.at(0)))
    {
        case 0:   url = "imaps://imap."+config.at(1)+"/"+config.at(2)+"/;UID=";
                  break;
        case 1:
                  break;
        default:  print_error(ca_error_type::INVALID_JSON);
                  break;
    }
}

bool emailhandle::parse_header(QString line,jsonhandle & json_file)
{

    if (line.isEmpty())
    {
        return false;
    }
    int pos = line.indexOf(":");
    if (pos != -1)
    {
        if (filter_keywords.indexOf(line.left(pos)) != -1)
        {
            json_file.set_string(line.left(pos),line.right((line.length()-pos)-2 ));
        }
        if (QString::compare(line.left(pos), "Content-Type", Qt::CaseSensitive) == 0)
        {
            pos = line.indexOf("boundary=");
            if (pos != -1)
            {
                boundaries << line.right((line.length()-pos)-9 );
            }


        }
    }
    return true;
}


void emailhandle::parse_email(QString data,QString file_path)
{
    jsonhandle json_file;
    if (data.isEmpty())
    {
        QTextStream(stdout) << "Warning: Corrupted data" << endl;
        return;
    }
    data.remove("\r");

    QStringList line_data = data.split("\n");
    bool header = true;

    QString message = "";
    QStringList messages;
    foreach (const QString &str, line_data)
    {

        if (header)
        {
            header = parse_header(str,json_file);
        }
        else
        {
            if (str.indexOf("--") != -1)
            {
                foreach (const QString &sstr, boundaries)
                {
                    if (str.indexOf(sstr) != -1)
                    {
                        if (!message.isEmpty())
                        {
                            messages << message;
                            message = "";
                        }
                        break;
                    }
                }
            }
            else if (str.indexOf("Content-transfer-encoding:") != -1)
            {
                if (!message.isEmpty())
                {
                    messages << message;
                    message = "";
                }
            }
            else if (str.indexOf("Content-Transfer-Encoding:") != -1)
            {
                if (!message.isEmpty())
                {
                    messages << message;
                    message = "";
                }
            }
            else if (str.indexOf("Content-Type:") != -1)
            {
                int pos = str.indexOf("boundary=");
                if (pos != -1)
                {
                    boundaries << str.right((str.length()-pos)-9 );
                }

                if (!message.isEmpty())
                {
                    messages << message;
                    message = "";
                }
            }
            else if (!str.isEmpty())
            {
                message += str + "\n";
            }
        }
    }
    if (!message.isEmpty())
    {
        messages << message;
        message = "";
    }
    json_file.set_string_array("Message",messages);
    json_file.write(file_path);
}

static size_t WriteCallback(void *contents, size_t size, size_t nmemb, void *userp)
{
    ((std::string*)userp)->append((char*)contents, size * nmemb);
    return size * nmemb;
}

QString emailhandle::curl_command(bool info,int UID)
{
    CURL *curl;
    CURLcode res = CURLE_OK;
    curl = curl_easy_init();
    QString qsbuffer = "";
    if(curl)
    {
        std::string readBuffer;
        curl_easy_setopt(curl, CURLOPT_USERNAME, user.toUtf8().constData());
        curl_easy_setopt(curl, CURLOPT_PASSWORD, pass.toUtf8().constData());

        if (info)
        {
            curl_easy_setopt(curl, CURLOPT_CUSTOMREQUEST,"EXAMINE INBOX");
        }

        curl_easy_setopt(curl, CURLOPT_URL, (url+QString::number(UID)).toUtf8().constData());

        curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0L);
        curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST, 0L);


        //curl_easy_setopt(curl, CURLOPT_VERBOSE, 1L);
        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, WriteCallback);
        curl_easy_setopt(curl, CURLOPT_WRITEDATA, &readBuffer);

        curl_easy_setopt(curl, CURLOPT_LOW_SPEED_LIMIT, 1000L);
        curl_easy_setopt(curl, CURLOPT_LOW_SPEED_TIME, 15L);
        curl_easy_setopt(curl, CURLOPT_CONNECTTIMEOUT, 10L);
        curl_easy_setopt(curl, CURLOPT_TIMEOUT, 15L);


        res = curl_easy_perform(curl);


        if(res != CURLE_OK)
        {
            print_error(ca_error_type::CURL_FAIL);
        }

        curl_easy_cleanup(curl);
        if (readBuffer.length() < 100000)
        {
            qsbuffer = QString::fromStdString(readBuffer);
        }
    }
    else
    {
        print_error(ca_error_type::CURL_FAIL);
    }
    return qsbuffer;
}


void emailhandle::get_all_email()
{
    int num = number_of_emails();
    int i;
    QTextStream(stdout) << "Number of email to be retrieved: " << num << endl;
    for (i=1;i<=num;++i)
    {
        parse_email( curl_command(false,i),(local_dir+QString::number(i)+".json"));
        QTextStream(stdout) << "Progess " << i << " of " << num << endl;
    }
}

int emailhandle::number_of_emails()
{
    QRegExp parse("\\* ([0-9]+) EXISTS");
    if(parse.indexIn(curl_command(true,1)) != -1)
    {
        return parse.cap(1).toInt();
    }
    print_error(ca_error_type::CURL_FAIL);
    return 0;
}

