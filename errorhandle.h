#ifndef ERRORHANDLE_H
#define ERRORHANDLE_H

#include <QObject>
#include <QString>
#include <QMap>
#include <QTextStream>

//Error messages
enum class ca_error_type {NO_ARGUMENTS, INVALID_FILE_PATH, NO_CONFIG,INVALID_JSON,INVALID_JSON_KEY,CURL_FAIL};

void print_error(ca_error_type input_type);
void print_error_message(ca_error_type input_type,QString message);

#endif // ERRORHANDLE_H
