
#include "errorhandle.h"
QMap<ca_error_type,QString> error_lookup = {
    {ca_error_type::NO_ARGUMENTS, "Error: No arguments." },
    {ca_error_type::INVALID_FILE_PATH, "Invalid file path." },
    {ca_error_type::NO_CONFIG, "No config file specified."},
    {ca_error_type::INVALID_JSON,"Invalid JSON file layout."},
    {ca_error_type::INVALID_JSON_KEY,"Invalid JSON key."},
    {ca_error_type::CURL_FAIL,"Failed to execute curl command."}};

void print_error(ca_error_type input_type)
{
    QTextStream(stdout) << (error_lookup[input_type]) << endl;
    exit(EXIT_FAILURE);
}

void print_error_message(ca_error_type input_type,QString message)
{
    QTextStream(stdout) << error_lookup[input_type] << endl;
    QTextStream(stdout) << message << endl;
    exit(EXIT_FAILURE);
}
