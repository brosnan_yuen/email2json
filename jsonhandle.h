#ifndef JSONHANDLE_H
#define JSONHANDLE_H

#include <QObject>
#include <QFile>
#include <QString>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonValue>
#include <QJsonParseError>
#include <QStringList>
#include <QJsonArray>

class jsonhandle : public QObject
{
    Q_OBJECT
    QJsonDocument document;
    QJsonValue get(QString value);
    void set(QString key, QJsonValue value);
public:
    jsonhandle();
    jsonhandle(QString file_path);
    void read(QString file_path); //Read JSON file into memory
    void write(QString file_path); //Write JSON file from memory
    void clear(); //Blank JSON file
    QString get_string(QString key); //Get string value for key
    int get_int(QString key); //Get int value for key
    void set_string(QString key,QString value); //Set key value pair for String
    void set_int(QString key,int value);//Set key value pair for int
    QStringList string_parse(QStringList list); //Get list of string
    QStringList get_string_array(QString key);
    void set_string_array(QString key,QStringList value);

signals:

public slots:

};

#endif // JSONHANDLE_H
