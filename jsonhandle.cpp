#include "jsonhandle.h"
#include "errorhandle.h"

jsonhandle::jsonhandle()
{
}

jsonhandle::jsonhandle(QString file_path)
{
    read(file_path);
}


void jsonhandle::read(QString file_path)
{
    QFile file(file_path);
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        print_error(ca_error_type::INVALID_FILE_PATH);
    }

    QJsonParseError error;
    document = QJsonDocument::fromJson(file.readAll(),&error);
    if (error.error != QJsonParseError::NoError)
    {
        print_error_message(ca_error_type::INVALID_JSON,file_path);
    }
    file.close();
}

QJsonValue jsonhandle::get(QString value)
{
     QJsonValue temp = document.object().value(value);
     if (temp == QJsonValue::Undefined)
     {
         print_error_message(ca_error_type::INVALID_JSON_KEY,value);
     }
     return temp;
}

QString jsonhandle::get_string(QString key)
{
     return get(key).toString();
}

int jsonhandle::get_int(QString key)
{
     return get(key).toInt();
}

void jsonhandle::clear()
{
    document = {};
}

void jsonhandle::set_string(QString key,QString value)
{
    QJsonValue jvalue(value);
    set(key,jvalue);
}

void jsonhandle::set_int(QString key,int value)
{
    QJsonValue jvalue(value);
    set(key,jvalue);
}

void jsonhandle::set(QString key, QJsonValue value)
{
    QJsonObject temp = document.object();
    temp[key] = value;
    document.setObject(temp);
}

void jsonhandle::write(QString file_path)
{
    QFile file(file_path);
    if (!file.open(QIODevice::WriteOnly))
    {
        print_error(ca_error_type::INVALID_FILE_PATH);
    }
    file.write(document.toJson(QJsonDocument::Indented));
    file.close();
}


QStringList jsonhandle::string_parse(QStringList list)
{
    QStringList parsed_list;

    foreach (const QString &str, list)
    {
        parsed_list << get_string(str);
    }
    return parsed_list;
}

QStringList jsonhandle::get_string_array(QString key)
{
    QJsonArray json_array = get(key).toArray();
    QStringList list;
    foreach (const QJsonValue & value, json_array)
    {
        list << value.toString();
    }
    return list;
}

void jsonhandle::set_string_array(QString key,QStringList value)
{
    QJsonArray json_array;
    foreach (const QString &str, value)
    {
        QJsonValue jvalue(str);
        json_array.append(jvalue);
    }
    set(key,json_array);
}





