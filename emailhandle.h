#ifndef EMAILHANDLE_H
#define EMAILHANDLE_H
#include <QObject>
#include <QString>
#include <stdio.h>
#include <curl/curl.h>
#include "jsonhandle.h"
#include <string>
#include <iostream>
#include <QRegExp>

class emailhandle  : public QObject
{
    Q_OBJECT
    QString url;
    QString pass;
    QString user;
    QString local_dir;
    QStringList filter_keywords;
    QStringList boundaries;
    QString curl_command(bool info, int UID);
    bool parse_header(QString line,jsonhandle & json_file);
    void parse_email(QString data, QString file_path);
public:
    emailhandle(QStringList config, QStringList filter_list);
    void get_all_email(); //Put all emails into JSON dir
    int number_of_emails();

signals:

public slots:

};

#endif // EMAILHANDLE_H
