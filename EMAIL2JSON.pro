#-------------------------------------------------
#
# Project created by QtCreator 2015-07-01T17:00:41
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = EMAIL2JSON
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app

QMAKE_CXXFLAGS += -std=c++11
LIBS += -lcurl

SOURCES += main.cpp \
    jsonhandle.cpp \
    errorhandle.cpp \
    emailhandle.cpp

HEADERS += \
    jsonhandle.h \
    errorhandle.h \
    emailhandle.h
